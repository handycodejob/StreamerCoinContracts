// Abstract contract for the full ERC 20 Token standard
// https://github.com/ethereum/EIPs/issues/20
pragma solidity ^0.4.18;

import "../Owned.sol";


contract StreamerTokenInterface is Owned {
    /* This is a slight change to the ERC20 base standard.
    function totalSupply() constant returns (uint256 supply);
    is replaced with:
    uint256 public totalSupply;
    This automatically creates a getter function for the totalSupply.
    This is moved to the base contract since public getter functions are not
    currently recognised as an implementation of the matching abstract
    function by the compiler.
    */

    /// Public variables of the token
    /// total amount of tokens
    uint256 public totalSupply;
    address public broker;
    mapping (string => mapping (string => address)) internal rolodex;

    /// modifiers
    modifier onlyBroker {
        require(msg.sender == broker);
        _;
    }

    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) public view returns (uint256 balance);

    /// @notice send `_value` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _value) public returns (bool success);

    /// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _value The amount of token to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);

    /// @notice `msg.sender` approves `broker` to spend `_value` tokens
    /// @param _value The amount of tokens to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(uint256 _value) public returns (bool success);

    /// @param _owner The address of the account owning tokens
    /// @return Amount of remaining tokens allowed to spent
    function allowance(address _owner) public view returns (uint256 remaining);

    /// @param _broker The address from which the balance will be retrieved
    /// @return Whether the transfer was successful or not
    function setBroker(address _broker) public onlyOwner returns (bool success);

    /// @notice register `_user` with `_address` to `_group`
    /// @param _group The group the user belongs to, normally platform
    /// @param _user The user id to link to the address
    /// @param _address The address that is getting linked to the user
    /// @return Whether setting the key was successful or not
    function register(string _group, string _user, address _address) public onlyBroker returns (bool success);

    /// @param _group The group the user belongs to, normally platform
    /// @param _user The user id who's addres you long for
    /// @return The address of the user
    function lookup(string _group, string _user) public view returns (address _address);

    /// @notice Create `_value` tokens and send it to `target`
    /// @param _target Address to receive the tokens
    /// @param _value the amount of tokens it will receive
    /// @return Whether the minting was successful
    function mintToken(address _target, uint256 _value) public onlyBroker returns (bool success);

    // solhint-disable-next-line no-simple-event-func-name  
    event Transfer(address indexed _from, address indexed _to, uint256 _value); 
    event Approval(address indexed _owner, uint256 _value);

    // solhint-disable-next-line no-simple-event-func-name  
    event Register(string indexed _group, string indexed _user, address _address);
    event Minted(address indexed _address, uint256 _value);

}
