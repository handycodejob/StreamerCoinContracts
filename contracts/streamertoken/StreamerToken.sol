/*
Implements StreamerToken token standard: https://github.com/ethereum/EIPs/issues/20
.*/


pragma solidity ^0.4.18;

import "./StreamerTokenInterface.sol";


contract StreamerToken is StreamerTokenInterface {

    uint256 constant private MAX_UINT256 = 2**256 - 1;
    mapping (address => uint256) public balances;
    mapping (address => uint256) public allowed;
    /*
    NOTE:
    The following variables are OPTIONAL vanities. One does not have to include them.
    They allow one to customise the token contract & in no way influences the core functionality.
    Some wallets/interfaces might not even bother to look at this information.
    */
    string public name;                   //fancy name: eg Simon Bucks
    uint8 public decimals;                //How many decimals to show.
    string public symbol;                 //An identifier: eg SBX

    function StreamerToken(
        uint256 _initialAmount,
        string _tokenName,
        uint8 _decimalUnits,
        string _tokenSymbol
    ) public {
        balances[msg.sender] = _initialAmount;               // Give the creator all initial tokens
        totalSupply = _initialAmount;                        // Update total supply
        name = _tokenName;                                   // Set the name for display purposes
        decimals = _decimalUnits;                            // Amount of decimals for display purposes
        symbol = _tokenSymbol;                               // Set the symbol for display purposes
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(balances[msg.sender] >= _value);
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        Transfer(msg.sender, _to, _value);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public onlyBroker returns (bool success) {
        uint256 allowance = allowed[_from];
        require(balances[_from] >= _value && allowance >= _value);
        balances[_to] += _value;
        balances[_from] -= _value;
        if (allowance < MAX_UINT256) {
            allowed[_from] -= _value;
        }
        Transfer(_from, _to, _value);
        return true;
    }

    function balanceOf(address _owner) public view returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(uint256 _value) public returns (bool success) {
        allowed[msg.sender] = _value;
        Approval(msg.sender, _value);
        return true;
    }

    function allowance(address _owner) public view returns (uint256 remaining) {
        return allowed[_owner];
    }   

    function setBroker(address _broker) public onlyOwner returns (bool success) {
        broker = _broker; 
        // delete allowed;  // TODO: Have to clear all allowances on broker change
        return true;
    }

    function register(string _group, string _user, address _address) public onlyBroker returns (bool success) {
        rolodex[_group][_user] = _address;
        Register(_group, _user, _address);
        return true;
    }

    function lookup(string _group, string _user) public view returns (address _address) {
       return rolodex[_group][_user];
    }

    function mintToken(address _target, uint256 _value) public onlyBroker returns (bool success) {
        balances[_target] += _value;
        totalSupply += _value;
        Minted(_target, _value);
        Transfer(broker, _target, _value);
        return true;
    }
}
