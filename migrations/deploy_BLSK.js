// Script to deploy BLSK
//

const StreamerTokenFactory = artifacts.require('./StreamerTokenFactory.sol');
const StreamerToken = artifacts.require('./StreamerToken.sol');

var metaF, metaB, meta;
var initialAmount = 1;
var name = "BlasKoin";
var decimals = 0;
var symbol = "BLSK";
var owner = "0x3a17a8f2216044bbd30c99f0f42a425e00e7b25b";
var broker = "0x0000fdd105049f380358088e43461c795e14948c";
var bartender = "0x7777437d570318762EFca9Efe163f111eAbD3EF0";

function transaction_formater(result) {
  var string = "\n";
  string = string + "  tx  : " + result.tx + "\n";
  string = string + "  from: " + result.receipt.from + "\n";
  string = string + "  to  : " + result.receipt.to;
  return string;
}


StreamerTokenFactory.deployed().then(function(instanceF) {
  metaF = instanceF;
  return metaF.createStreamerToken(initialAmount, name, decimals, symbol, {from: owner});
}).then(function(resultF) {
  // If this callback is called, the transaction was successfully processed.
  console.log("Token Contract deploy successful!", transaction_formater(resultF))

  StreamerToken.deployed().then(function(instance) {
    meta = instance;
    return meta.sendTransaction({
        value: web3.toWei(1, "ether"),
        from: owner,
        to: broker
      })
    }).then(function(result) {
      // If this callback is called, the transaction was successfully processed.
      console.log("Ether sent to broker correctly", transaction_formater(result))
    }).catch(function(e) {
      // There was an error! Handle it.
      console.log("Error with sending the broker coins", e)
  })

  StreamerToken.deployed().then(function(instance) {
    meta = instance;
    return meta.setBroker(broker, {from: owner});
    }).then(function(result) {
      // If this callback is called, the transaction was successfully processed.
      console.log("Token broker set successful!", transaction_formater(result))
    }).catch(function(e) {
      // There was an error! Handle it.
      console.log("Error with broker setting", e)
  })

  StreamerToken.deployed().then(function(instance) {
    metaB = instance;
    return meta.register('special', 'Bartender', bartender, {from: broker});
    }).then(function(result) {
      // If this callback is called, the transaction was successfully processed.
      console.log("Bartneder set successful!", transaction_formater(result))
    }).catch(function(e) {
      // There was an error! Handle it.
      console.log("Error with bartending setting", e)
  })

}).catch(function(e) {
  // There was an error! Handle it.
  console.log("Error with deploying token: ", e)
})
