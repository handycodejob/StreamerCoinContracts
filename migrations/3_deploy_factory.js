const StreamerTokenFactory =
  artifacts.require('./StreamerTokenFactory.sol');

module.exports = (deployer) => {
  deployer.deploy(StreamerTokenFactory);
};
