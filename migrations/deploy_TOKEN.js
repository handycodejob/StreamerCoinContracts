// Template for deploying a coin
//

const StreamerTokenFactory = artifacts.require('./StreamerTokenFactory.sol');
const StreamerToken = artifacts.require('./StreamerToken.sol');

var metaF, meta;
var initialAmount = 1;
var name = "";
var decimals = 0;
var symbol = "";
var owner = "";
var broker = "";

StreamerTokenFactory.deployed().then(function(instanceF) {
  metaF = instanceF;
  return metaF.createStreamerToken(initialAmount, name, decimals, symbol, {from: owner});
}).then(function(resultF) {
  // If this callback is called, the transaction was successfully processed.
  alert("Token Contract deploy successful!", resultF)

  StreamerToken.deployed().then(function(instance) {
    meta = instance;
    return meta.setBroker(broker, {from: owner});
  }).then(function(result) {
      // If this callback is called, the transaction was successfully processed.
      alert("Token broker set successful!", result)
    }).catch(function(e) {
      // There was an error! Handle it.
      alert("Error with broker setting", e)
  })
}).catch(function(e) {
  // There was an error! Handle it.
  alert("Error with ", e)
})
