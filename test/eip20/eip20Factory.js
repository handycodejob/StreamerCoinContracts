const StreamerTokenFactory = artifacts.require('StreamerTokenFactory');

contract('StreamerTokenFactory', (accounts) => {
  it('Verify a Human Standard Token once deployed using both verification functions.', async () => {
    const factory = await StreamerTokenFactory.new();
    const newTokenAddr = await factory.createStreamerToken.call(100000, 'Simon Bucks', 2, 'SBX', { from: accounts[0] });
    await factory.createStreamerToken(100000, 'Simon Bucks', 2, 'SBX', { from: accounts[0] });
    const res = await factory.verifyStreamerToken.call(newTokenAddr, { from: accounts[0] });
    assert(res, 'Could not verify the token.');
  });
});
